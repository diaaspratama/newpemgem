using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class leentweenlogo : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform logo;
    public float animduration;
    public Ease animease;
    
    void Start ()
    {
        logo
            .DOMoveY(3f, animduration)
            .SetEase(animease);
    }

}
